const AXIOS_CONFIG = {
  baseURL: process.env.apiUrl,
}

export default ({ $axios }) => {
  Object.assign($axios.defaults, AXIOS_CONFIG)
}
