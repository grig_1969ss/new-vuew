export const state = () => ({
  user: {},
  userData: {},

})
export const getters = {
  USER: state => state.user,
  USER_DATA: state => state.userData,

}
export const actions = {
  getUser({ commit }, param) {
    return new Promise((resolve, reject) => {
      this.$axios.post('/v1/users/sign_up', param)
        .then(response => {
          resolve(response)
          console.log("res = ", res)
        })
        .catch(error => {
          reject(error.response.data.errors)
          console.log("error = ", error)
        })
    })
    // commit('SET_USER', data)
  },
  async getUserIp({commit}) {
   const IP = await this.$axios.get('https://ipapi.co/json/')
    return IP
  },
  async getCoutries() {
    const Countries = await  this.$axios.get( "http://5.188.131.23/api/v1/places/countries?country=%&limit=300")
    return  Countries;

  },
  async getCityes({ commit },id) {
    const Cityes = await  this.$axios.get( `http://5.188.131.23/api/v1/places/${id}/cities?city=%&limit=1000`)
    return  Cityes;

  },
   async signIn({commit}, params) {
    const SignIn = await this.$axios.post('/v1/users/sign_in', params)
     commit('SET_USER_DATA', SignIn)
     return SignIn;
    window.localStorage.setItem("userData",SignIn);

   }
}
export const mutations = {
  SET_USER(state, value){
    state.user = value
  },
  SET_USER_DATA(state, value){
    state.userData = value.data.data;
  },


}
