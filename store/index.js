export const state = () => ({
  binars: {},
  contracts: {},
  currencies: {},
  ranks: {},
  settings: {},
  transactionTypes: {},
  userBinars: {},
  userContracts: {},
  userRanks: {},
  userReferalLinks: {},
  userTransactions: {},
  users: {},
})
export const getters = {
  BINARS: state => state.binars,
  CONTRACTS: state => state.contracts,
  CURRENCIES: state => state.currencies,
  RANKS: state => state.ranks,
  SETTINGS: state => state.settings,
  TRANSACTION_TYPES: state => state.transactionTypes,
  USER_BINARS: state => state.userBinars,
  USER_CONTRACTS: state => state.userContracts,
  USER_RANKS: state => state.userRanks,
  USER_REFERAL_LINKS: state => state.userReferalLinks,
  USER_TRANSACTIONS: state => state.userTransactions,
  USERS: state => state.users,
}
export const actions = {
  async getBinars({ commit }) {
    const data = await this.$axios.get('/binars')
    commit('SET_BINARS', data)
  },
  async getContracts({ commit }) {
    const data = await this.$axios.get('/contracts')
    commit('SET_CONTRACTS', data)
  },
  async getCurrencies({ commit }) {
    const data = await this.$axios.get('/currencies')
    commit('SET_CURRENCIES', data)
  },
  async getRanks({ commit }) {
    const data = await this.$axios.get('/ranks')
    commit('SET_RANKS', data)
  },
  async getSettings({ commit }) {
    const data = await this.$axios.get('/settings')
    commit('SET_SETTINGS', data)
  },
  async getTransactionTypes({ commit }) {
    const data = await this.$axios.get('/transaction_types')
    commit('SET_USER_BINARS', data)
  },
  async getUserBinars({ commit }) {
    const data = await this.$axios.get('/user_binars')
    commit('SET_TRANSACTION_TYPES', data)
  },
  async getUserContracts({ commit }) {
    const data = await this.$axios.get('/user_contracts')
    commit('SET_USER_CONTRACTS', data)
  },
  async getUserRanks({ commit }) {
    const data = await this.$axios.get('/user_ranks')
    commit('SET_USER_RANKS', data)
  },
  async getUserReferalLinks({ commit }) {
    const data = await this.$axios.get('/user_referal_links')
    commit('SET_USER_REFERAL_LINKS', data)
  },
  async getUserTransactions({ commit }) {
    const data = await this.$axios.get('/user_transactions')
    commit('SET_USER_TRANSACTIONS', data)
  },
  async getUsers({ commit }) {
    const data = await this.$axios.get('/users')
    commit('SET_USERS', data)
  },
}
export const mutations = {
  SET_BINARS(state, value){
    state.binars = value
  },
  SET_CONTRACTS(state, value){
    state.contracts = value
  },
  SET_CURRENCIES(state, value){
    state.currencies = value
  },
  SET_RANKS(state, value){
    state.ranks = value
  },
  SET_SETTINGS(state, value){
    state.settings = value
  },
  SET_TRANSACTION_TYPES(state, value){
    state.transactionTypes = value
  },
  SET_USER_BINARS(state, value){
    state.userBinars = value
  },
  SET_USER_CONTRACTS(state, value){
    state.userContracts = value
  },
  SET_USER_RANKS(state, value){
    state.userRanks = value
  },
  SET_USER_REFERAL_LINKS(state, value){
    state.userReferalLinks = value
  },
  SET_USER_TRANSACTIONS(state, value){
    state.userTransactions = value
  },
  SET_USERS(state, value){
    state.users = value
  },
}
